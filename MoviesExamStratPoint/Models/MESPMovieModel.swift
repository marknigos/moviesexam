//
//  MESPMovieModel.swift
//  MoviesExamStratPoint
//
//  Created by ML on 11/11/2017.
//  Copyright © 2017 Mark Nigos. All rights reserved.
//

import UIKit

class MESPMovieModel: NSObject {
    //response
    var genres: [String]!
    var imdbCode: String!
    var language: String!
    var movieID: Int!
    var mpaRating: String!
    var overview: String!
    var rating: Double!
    var runtime: Int!
    var slug: String!
    var state: String!
    var title: String!
    var titleLong: String!
    var url: String!
    var year: Int!
    
    //for movie images
    var movieBackdrop: UIImage!
    var moviePoster: UIImage!
    
    init(data : [String : Any]){
        super.init()
        genres    = data["genres"] as! [String]
        imdbCode  = data["imdb_code"] as! String
        language  = data["language"] as! String
        movieID   = data["id"] as! Int
        mpaRating = data["mpa_rating"] as! String
        overview  = data["overview"] as! String
        rating    = data["rating"] as! Double
        runtime   = data["runtime"] as! Int
        state     = data["state"] as! String
        slug      = data["slug"] as! String
        title     = data["title"] as! String
        titleLong = data["title_long"] as! String
        url       = data["url"] as! String
        year      = data["year"] as! Int
        
        let backdropURL = URL(string: "http://cayaco.info/movielist/images/\(slug!)-backdrop.jpg")
        let backdropData = try? Data(contentsOf: backdropURL!)
        movieBackdrop = UIImage(data: backdropData!)
        
        let posterURL = URL(string: "http://cayaco.info/movielist/images/\(slug!)-cover.jpg")
        let posterData = try? Data(contentsOf: posterURL!)
        moviePoster = UIImage(data: posterData!)
        
    }

}
