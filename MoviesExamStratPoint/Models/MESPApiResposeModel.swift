//
//  MESPApiResposeModel.swift
//  MoviesExamStratPoint
//
//  Created by ML on 11/11/2017.
//  Copyright © 2017 Mark Nigos. All rights reserved.
//

import UIKit

class MESPApiResposeModel: NSObject {
    var status: String!
    var data: [String : Any]? = nil
    var statusMessage: String!
    
    init(data : Dictionary<String, Any>){
        super.init()
        status        = data["status"] as! String
        self.data     = data["data"] as? [String : Any]
        statusMessage = data["status_message"] as! String
    }

}
