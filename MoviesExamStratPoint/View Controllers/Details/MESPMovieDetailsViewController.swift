//
//  MESPMovieDetailsViewController.swift
//  MoviesExamStratPoint
//
//  Created by ML on 11/11/2017.
//  Copyright © 2017 Mark Nigos. All rights reserved.
//

import UIKit

class MESPMovieDetailsViewController: UIViewController {
    //MARK: - Outlets
    @IBOutlet weak var movieBackdropImg: UIImageView!
    @IBOutlet weak var moviePosterImg: UIImageView!
    @IBOutlet weak var movieTitleLbl: UILabel!
    @IBOutlet weak var movieYrReleasedLbl: UILabel!
    @IBOutlet weak var movieRatingLbl: UILabel!
    @IBOutlet weak var movieOverviewTxtView: UITextView!
    
    //MARK: - Variables
    var selectedMovie: MESPMovieModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = selectedMovie.title!
        
        movieBackdropImg.image = selectedMovie.movieBackdrop
        moviePosterImg.image = selectedMovie.moviePoster
        
        movieTitleLbl.text = selectedMovie.title!
        movieYrReleasedLbl.text = "Year Released: \(selectedMovie.year!)"
        movieRatingLbl.text = "Rating: \(selectedMovie.rating!)"
        movieOverviewTxtView.text = selectedMovie.overview!
        
    }
}
