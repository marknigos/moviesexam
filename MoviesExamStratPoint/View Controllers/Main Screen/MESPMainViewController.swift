//
//  MESPMainViewController.swift
//  MoviesExamStratPoint
//
//  Created by ML on 11/11/2017.
//  Copyright © 2017 Mark Nigos. All rights reserved.
//

import UIKit

class MESPMainViewController: UIViewController {
    //MARK: - Outlets
    @IBOutlet weak var moviesTableView: UITableView!
    @IBOutlet weak var loadingView: UIView!

    //MARK: - Variables
    var moviesArray: [MESPMovieModel] = []
    
    //MARK: VC Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getMovieList()
    }
    
    //MARK: Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showMovieDetails"{
            guard let detailsView = segue.destination as? MESPMovieDetailsViewController,
                let selectedIndex = moviesTableView.indexPathForSelectedRow?.row
                else { return }
            
            detailsView.selectedMovie = moviesArray[selectedIndex]
        }
    }
    
    //MARK: Custom Methods
    func getMovieList(){
        
        let urlString = URL(string: "https://cayaco.info/movielist/list_movies_page1.json")
        if let url = urlString {
            let urlRequest = URLRequest(url: url)
            let config = URLSessionConfiguration.default
            //            let session = URLSession(configuration: config)
            let session = URLSession(configuration: config, delegate: self as URLSessionDelegate, delegateQueue: nil)
            
            let task = session.dataTask(with:urlRequest, completionHandler: {(data, response, error) in
                guard let data = data, error == nil else { return }
                
                do {
                    let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String:Any]
                    let dataResponse = MESPApiResposeModel(data: json)
                    
                    if dataResponse.status != "ok" { //better if we have boolean
                        MESPHelper.displayAlert(withTitle: dataResponse.statusMessage)
                    } else {
                        if dataResponse.data == nil {
                            MESPHelper.displayAlert(withTitle: dataResponse.statusMessage)
                        } else {
                            var movieList = [MESPMovieModel]()
                            for movie in dataResponse.data!["movies"] as! NSArray {
                                movieList.append(MESPMovieModel(data: movie as! [String : Any]))
                            }
                            
                            DispatchQueue.main.async {
                                self.loadingView.removeFromSuperview()
                                self.moviesTableView.isHidden = false
                                self.moviesArray = movieList
                                self.moviesTableView.reloadData()
                            }
                        }
                    }
                    
                } catch let error as NSError {
                    print(error)
                }
            })
            task.resume()
        }
    }
}

//MARK: - UITableView DataSource
extension MESPMainViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return moviesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MESPMainTableViewCellID") as! MESPMainTableViewCell
        cell.configureDisplay(withData: moviesArray[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}

//MARK: - URLSessionDelegate
//Added this for the SSL
extension MESPMainViewController: URLSessionDelegate {
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        if challenge.protectionSpace.authenticationMethod == NSURLAuthenticationMethodServerTrust {
            if challenge.protectionSpace.host == "cayaco.info" {
                let credentials = URLCredential(trust: challenge.protectionSpace.serverTrust!)
                completionHandler(URLSession.AuthChallengeDisposition.useCredential, credentials)
            }
        }
    }
}

