//
//  MESPHelper.swift
//  MoviesExamStratPoint
//
//  Created by ML on 11/11/2017.
//  Copyright © 2017 Mark Nigos. All rights reserved.
//

import UIKit

class MESPHelper: NSObject {
    class func displayAlert(withTitle title: String? = "", withMessage message: String? = "" ){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        var rootViewController = UIApplication.shared.keyWindow?.rootViewController
        
        if let navigationController = rootViewController as? UINavigationController {
            rootViewController = navigationController.viewControllers.first
        }
        
        rootViewController?.present(alertController, animated: true, completion: nil)
    }

}
