//
//  MESPMainTableViewCell.swift
//  MoviesExamStratPoint
//
//  Created by ML on 11/11/2017.
//  Copyright © 2017 Mark Nigos. All rights reserved.
//

import UIKit

class MESPMainTableViewCell: UITableViewCell {
    //MARK: - Outlets
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var movieBackdrop: UIImageView!
    @IBOutlet weak var movieTitle: UILabel!
    @IBOutlet weak var movieYearReleased: UILabel!
    
    //MARK: Custom Method
    func configureDisplay(withData data: MESPMovieModel){
        movieBackdrop.image = data.movieBackdrop
        movieTitle.text = data.title
        movieYearReleased.text = "Year Released: \(data.year!)"
    }

}
